Img = React.createClass({
    render() {
        return (
            <div style={{display: "inline"}}>
        <img ref="img" onMouseLeave={this.off} onMouseOver={this.hover} style={this.styling()} className="img-responsive" src={this.props.src} alt=""/> 
            </div>
        
           
        )
    },
    styling() {
        return {
            height: "29vh",
            width: "29vw",
            padding: "10px"

            
        }
    },
    
    hover() {
        var $i = $(this.refs.img)
        
        $i.animate({padding: "+=10"})
        $i.css({border: "1px solid white"})
        
    },
    
    off() {
         var $i = $(this.refs.img)
        
        $i.animate({padding: "-=10"})
        $i.css({border: "none"})
    }   
});


