FullBleed = React.createClass({
    getInitialState() {

        return {
            winWidth: typeof window !== 'undefined' ? window.innerWidth : '',
            winHeight: typeof window !== 'undefined' ? window.innerHeight : '',
            isMobile: typeof window !== 'undefined' && window.innerHeight < 768
        }
    },

    render() {
        return (
            <div style={{height: "100vh", marginBottom: "-10px"}}>
                <div className="fullbleed"
                     ref="fullbleedContainer"
                     style={this._getContainerStyle()}>
                </div>
                <Article heading={this.props.heading}
                         body={this.props.body}
                         position={this.props.position}/>

            </div>
        );
    },

    _getContainerStyle() {
        return {
            marginTop: '8px',
            background: 'url(' + this.props.imgPath + ')',
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: "50% 0",
            minHeight: '' + this.state.winHeight + 'px',
            width: '' + this.state.winWidth + 'px',
            display: 'block',
            paddingRight: '0',
            paddingLeft: '0',
            position: 'relative',
            top: '-8px',
            backgroundAttachment: "fixed"
        }

    }
}); //End of Fullbleed

Article = React.createClass({
    render() {
        return (
            <div className="article"
                 id="fullbleed-text"
                 style={this._getPosition(this.props.position)}>
                <h1 id="fullbleed-h1" style={{color: "#FFFAE8"}}>{this.props.heading}</h1>
                <p id="fullbleed-body" style={{color: "#FFFAE8"}}>{this.props.body}</p>
            </div>

        )
    },

    componentDidMount() {
      $(".article").animate({
          paddingLeft: "+=100px",
          opacity: 1
      },1000)
    },

    _getPosition() {
        switch (this.props.position) {
            case "topCenter":
                return {
                    textAlign: "center",
                    bottom: "610px",
                    position: "relative"
                };
            case "topRight":
                return {
                    position: 'relative',
                    textAlign: "right",
                    bottom: '40rem',
                    right: '6rem'
                };
            case "topLeft":
                return {
                    position: 'relative',
                    textAlign: "left",
                    bottom: '40rem',
                    left: '6rem'
                };
            case "centerCenter":

                return {
                    position: 'relative',
                    textAlign: "center",
                    bottom: '33rem',
                    background: "rgba(0,0,0,0.6)",
                    opacity: "0"
                };
            case "centerLeft":
                return {
                    position: 'relative',
                    textAlign: "left",
                    bottom: '27rem',
                    left: '6rem'

                };
            case "centerRight":
                return {
                    position: 'relative',
                    textAlign: "right",
                    bottom: '27rem',
                    right: '6rem'
                };
            case "bottomCenter":
                return {
                    textAlign: "center",
                    bottom: "6rem",
                    position: "relative"
                };
            case "bottomLeft":
                return {
                    position: 'relative',
                    textAlign: "left",
                    bottom: '6rem',
                    left: '6rem'
                };
            case "bottomRight":
                return {
                    position: 'relative',
                    textAlign: 'right',
                    bottom: '6rem',
                    right: '6rem'
                };
            default:
                return {
                    position: 'relative',
                    textAlign: "center",
                    bottom: '26rem'
                }
        }
    }
}); //End of Article