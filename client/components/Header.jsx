Header = React.createClass({

    css: {
        heading: {
            letterSpacing: "27.8px",
            color: "white",
            textAlign: "center"
        }
    },
         
    render() {
        return (
                <h1 style={this.css.heading}>Reactive Gallery</h1>
        )
    }
})

