Preloader = React.createClass({

    css: {
        preload: {
            zIndex: "3",
            background: "white",
            height: "100vh",
            width: "100vw",
            position: "fixed",
            top: "0",
            textAlign: "center"
        },
        
        loader: {
            background: "black",
            height: "50px",
            width: "1px",
            marginTop: "100px"
        }
    },
         
    render() {
        return (
            <div ref="pre" className="text-center" style={this.css.preload}>
                <h1>Thanks for visiting!</h1>
                       <div style={this.css.loader} ref="loader" id="loader"></div>         

                <small onMouseOver={this.close}>Close</small>
            </div>
        )
    },
    
    close() {

        

    },
    
    componentDidMount() {
        var $l = $(this.refs.loader)
        var $p = $(this.refs.pre)
        
        $l.animate({width: "100vw"},4000,function(){
                    $p.animate({top: "-=100vh",opacity: "0"},2000)
        })
    }
})

