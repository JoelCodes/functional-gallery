Gallery = React.createClass({
          propTypes: {

            images: React.PropTypes.array
    
         },
         
    render() {
        return (
            <div>
               {this.createGallery()}
            </div>
        )
    },
    
    createGallery() {
        
        return this.props.images.map(function(image, index) {
            return <Img key={image} src={image} />
        })
    }
})

